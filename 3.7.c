#include <string.h>
#include <stdio.h>




int main()
{
	unsigned char s[256];
	int counts[256] = { 0 };
	int i = 0, j = 0, f = 0, check = 1;
	char t = 0;

	puts("Enter a string: ");
	fgets(s, 256, stdin);
	s[strlen(s) - 1] = 0;


	for (i = 0; i < strlen(s); i++)
	{
		j = s[i];
		counts[j]++;
	}
	
	for (i = 0; i < strlen(s); i++)
	{
			t = maxfinder(counts); //store maximum in a temporary variable 
			check = counts[t];   
			if (check != 0) //print only if element is not equal to zero
			printf("'%c' = %d \n", t, counts[t]);
			counts[t] = 0;
		
	}
			
	return 0;
}

int maxfinder(int counts[])  //function for finding maximal number of occurrence letter
{
	int i, max = 0, pmax = 0;
	for (i = 0; i < 256; i++)
	{
		if (counts[i] != 0)
			if (max < counts[i])
			{
				max = counts[i];
				pmax = i;
			}
	}
	return pmax; 
}
