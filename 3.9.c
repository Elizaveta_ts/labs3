#include <stdio.h>
#include <string.h>

int main()
{
	char buf[256];
	int i = 0, count = 0;
	int max = 0, first = 0, sequence = 0;
	printf("Enter the string, please: \n");
	fgets(buf, 256, stdin);
	buf[strlen(buf) - 1] = 0;

	while (buf[i])
	{
		if (buf[i] == buf[i + 1])  // sequence started
		{
			sequence = 1;
			count++;
		}
		if (buf[i] != buf[i + 1] && sequence == 1) //sequence finished
		{
			count++;
			if (max < count)
			{
				max = count; //set into memory number of letters
				first = i - count + 1; //set into memory pozition of first letter in sequence
			}
			sequence = 0;
			count = 0;
		}
		i++;
	}
	printf("number of letters in maximal sequence:%d, maximal sequence: ", max); //print number of maximal sequence
	for (i = first; i < first + max; i++) //print sequence
		putchar(buf[i]);
	putchar('\n');
	return 0;
}