#include <stdio.h>
#include <string.h>

int main()
{
	unsigned char buf[256];
	int beg = 0, fin = 0;
	int count = 0;
	int i = 0, inWord = 0, len = 0, word =0;
	int n;
	puts("Enter a string: ");
	fgets(buf, 256, stdin);
	buf[strlen(buf) - 1] = 0;
	puts("Enter a number of word: ");
	scanf("%d", &n);

	if (n > count)
		printf("\nError! Your number can not be greater than the number of words\n");
	else if (n == 0)
		printf("\nError! Your number can not be equal to zero\n");
	else if (n < 0)
		printf("\nError! Your number can not be negative\n");

	while (buf[i])
	{
		if (buf[i] != ' ' && inWord == 0) //word begin
		{
			count++;
			if (count == n)
				beg = i; //here our word is begin

			inWord = 1;
		}
		else if (buf[i] == ' ' && inWord == 1) //word finish
		{
			inWord = 0;
		}
		i++;
		if (count == n)
			fin = i;// here our word is finish
	}
	if (n < count)
	{
		for (i = 0; i < beg; i++)
			putchar(buf[i]);
		for (i = fin; i < strlen(buf); i++)
			putchar(buf[i]);
		putchar('\n');
	}

	return 0;
}