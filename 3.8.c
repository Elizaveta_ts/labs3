#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main()
{
	unsigned char buf[256];
	int count = 0;
	int i = 0, inWord = 0, upper = 0;
	int n;
	puts("Enter a string: ");
	fgets(buf, 256, stdin);
	buf[strlen(buf) - 1] = 0;
	puts("Enter a number of word: ");
	scanf("%d", &n);

	if (n < count)
		printf("\nError! Your number can not be greater than the number of words\n");
	else if (n == 0)
		printf("\nError! Your number can not be equal to zero\n");
	else if (n < 0)
		printf("\nError! Your number can not be negative\n");
	
	
	while (buf[i])
		{
			if (buf[i] != ' ' && inWord == 0) //word begin
			{
				count++;
				inWord = 1;
			}
			else if (buf[i] == ' ' && inWord == 1) //word finish
				inWord = 0;
			if (count == n)
				putchar(buf[i]);
			i++;
		}


	return 0;
}