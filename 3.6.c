#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10

int main()
{
	int sum = 0, min = 0, minp = 0, max = 0,maxp = 0;
	int arr[N];
	int i = 0;

	srand(time(0));

	for (i = 0; i < N; i++) //in this cycle we are creating a massive of integer numbers
	{
		arr[i] = (rand() - RAND_MAX / 2) % 100;
		printf("%d ", arr[i]);
	}

	for (i = 0; i < N; i++)
	{
		if (arr[i] > max)
		{
			max = arr[i];
			maxp = i;
		}
		if (arr[i] < min)
		{
			min = arr[i];
			minp = i;
		}
	}

	if (minp < maxp)
	{
		for (i = minp + 1; i < maxp; i++) //counting sum between minimal and maximal numbers
			sum += arr[i];
	}

	else if (maxp < minp)
	{
		for (i = maxp + 1; i < minp; i++) //counting sum between maximal and minimal numbers
			sum += arr[i];
	}
	printf("\nsum: %d\n", sum);
	return 0;
}