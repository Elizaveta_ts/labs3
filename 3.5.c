#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10

int main()
{
	int sum = 0, firstNeg = 0, lastPoz = 0 ;
	int arr[N];
	int i = 0;

	srand(time(0));

	for (i = 0; i < N; i++) //in this cycle we are creating a massive of integer numbers
	{
		arr[i] = (rand() - RAND_MAX / 2) % 100;
		printf("%d ", arr[i]);
	}
	for(i = 0; i < N; i++) //searching first negative number
		if (arr[i] < 0)
		{
			firstNeg = i;
			break;
		}
	for (i = N-1; i >= 0; i--) //searching last positive number
		if (arr[i] > 0)
		{
			lastPoz = i;
			break;
		}
	if (firstNeg >= lastPoz) //counting sum between first negative and last positive
		return 1;
	for (i = firstNeg+1; i < lastPoz; i++)
		sum += arr[i];

		printf("\nsum: %d\n", sum);
	return 0;
}
